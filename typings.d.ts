import * as Knex from 'knex';

declare global {
    namespace Express {
        export interface Request {
            db: Knex;
        }

        export interface Application {
            port: any;
        }
    }
}