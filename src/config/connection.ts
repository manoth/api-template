import * as Knex from 'knex';
import { MySqlConnectionConfig } from 'knex';

import * as path from 'path';
const fs = require('fs');
const pathFile = path.join(__dirname, '../../config.json');

export class Connection {
    config: any = JSON.parse(fs.readFileSync(pathFile, 'utf8'));
    db() {
        let connection: MySqlConnectionConfig = this.config.connection;
        let client: string = this.config.client;
        return Knex({
            client: client,
            connection: connection,
            pool: {
                min: 0,
                max: 7,
                afterCreate: (conn, done) => {
                    conn.query('SET NAMES utf8', (err) => {
                        done(err, conn);
                    });
                }
            },
            debug: true,
            acquireConnectionTimeout: 10000
        });
    }
}